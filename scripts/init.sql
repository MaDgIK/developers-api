CREATE TABLE IF NOT EXISTS registered_service
(
    id                        integer                NOT NULL,
    client_id                 character varying(255) NOT NULL,
    owner                     character varying(255) NOT NULL,
    name                      character varying(255) NOT NULL,
    creation_date             timestamp without time zone NOT NULL,
    registration_access_token text                   NOT NULL,
    key_type                  character varying(255)
);

CREATE SEQUENCE IF NOT EXISTS registered_service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE registered_service_id_seq OWNED BY registered_service.id;

ALTER TABLE ONLY registered_service ALTER COLUMN id SET DEFAULT nextval('registered_service_id_seq'::regclass);

ALTER TABLE ONLY registered_service DROP CONSTRAINT IF EXISTS registered_service_pkey;

ALTER TABLE ONLY registered_service ADD CONSTRAINT registered_service_pkey PRIMARY KEY (id);

