ALTER TABLE ONLY registered_service ALTER COLUMN client_id type TEXT;
ALTER TABLE ONLY registered_service ALTER COLUMN owner type TEXT;
ALTER TABLE ONLY registered_service ALTER COLUMN name type TEXT;
ALTER TABLE ONLY registered_service ALTER COLUMN key_type type TEXT;
ALTER TABLE ONLY registered_service ALTER COLUMN url type TEXT;
ALTER TABLE ONLY registered_service ALTER COLUMN description type TEXT;
ALTER TABLE ONLY registered_service ALTER COLUMN frequency type TEXT;
ALTER TABLE ONLY registered_service ALTER COLUMN target type TEXT;
ALTER TABLE ONLY registered_service ALTER COLUMN issuer type TEXT;
ALTER TABLE ONLY registered_service ALTER COLUMN contacts type TEXT;

ALTER TABLE ONLY personal_info ALTER COLUMN id type TEXT;
ALTER TABLE ONLY personal_info ALTER COLUMN affiliation type TEXT;
ALTER TABLE ONLY personal_info ALTER COLUMN email type TEXT;
ALTER TABLE ONLY personal_info ALTER COLUMN name type TEXT;
ALTER TABLE ONLY personal_info ALTER COLUMN position type TEXT;
ALTER TABLE ONLY personal_info ALTER COLUMN surname type TEXT;
ALTER TABLE ONLY personal_info ALTER COLUMN affiliation_type type TEXT;
