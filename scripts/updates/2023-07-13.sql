ALTER TABLE ONLY personal_info ADD COLUMN affiliation_type character varying(255) NOT NULL;
ALTER TABLE ONLY registered_service ADD COLUMN description character varying(255) NOT NULL DEFAULT '';
ALTER TABLE ONLY registered_service ADD COLUMN frequency character varying(255);
ALTER TABLE ONLY registered_service ADD COLUMN target character varying(255) NOT NULL DEFAULT '';
ALTER TABLE ONLY registered_service ADD COLUMN issuer character varying(255) NOT NULL DEFAULT 'https://aai.openaire.eu/oidc/register';
