ALTER TABLE ONLY registered_service ADD COLUMN url character varying(255) NOT NULL DEFAULT '';

CREATE TABLE IF NOT EXISTS personal_info
(
    id          character varying(255) NOT NULL,
    affiliation character varying(255) NOT NULL,
    email       character varying(255) NOT NULL,
    name        character varying(255) NOT NULL,
    "position"  character varying(255) NOT NULL,
    surname     character varying(255) NOT NULL
);

ALTER TABLE ONLY personal_info DROP CONSTRAINT IF EXISTS personal_info_pkey;
ALTER TABLE ONLY personal_info ADD CONSTRAINT personal_info_pkey PRIMARY KEY (id);
