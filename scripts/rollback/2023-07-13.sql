ALTER TABLE ONLY personal_info DROP COLUMN affiliation_type;
ALTER TABLE ONLY registered_service DROP COLUMN description;
ALTER TABLE ONLY registered_service DROP COLUMN frequency;
ALTER TABLE ONLY registered_service DROP COLUMN target;
ALTER TABLE ONLY registered_service DROP COLUMN issuer;
