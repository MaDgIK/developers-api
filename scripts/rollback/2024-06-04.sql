ALTER TABLE ONLY registered_service ALTER COLUMN client_id type character varying(255);
ALTER TABLE ONLY registered_service ALTER COLUMN owner type character varying(255);
ALTER TABLE ONLY registered_service ALTER COLUMN name type character varying(255);
ALTER TABLE ONLY registered_service ALTER COLUMN key_type type character varying(255);
ALTER TABLE ONLY registered_service ALTER COLUMN url type character varying(255);
ALTER TABLE ONLY registered_service ALTER COLUMN description type character varying(255);
ALTER TABLE ONLY registered_service ALTER COLUMN frequency type character varying(255);
ALTER TABLE ONLY registered_service ALTER COLUMN target type character varying(255);
ALTER TABLE ONLY registered_service ALTER COLUMN issuer type character varying(255);
ALTER TABLE ONLY registered_service ALTER COLUMN contacts type character varying(255);

ALTER TABLE ONLY personal_info ALTER COLUMN id type character varying(255);
ALTER TABLE ONLY personal_info ALTER COLUMN affiliation type character varying(255);
ALTER TABLE ONLY personal_info ALTER COLUMN email type character varying(255);
ALTER TABLE ONLY personal_info ALTER COLUMN name type character varying(255);
ALTER TABLE ONLY personal_info ALTER COLUMN position type character varying(255);
ALTER TABLE ONLY personal_info ALTER COLUMN surname type character varying(255);
ALTER TABLE ONLY personal_info ALTER COLUMN affiliation_type type character varying(255);
