package eu.dnetlib.developers.entities;

import eu.dnetlib.developers.dto.ServiceForm;
import eu.dnetlib.developers.dto.ServiceRequest;
import eu.dnetlib.developers.dto.ServiceResponse;
import eu.dnetlib.developers.utils.StringListConverter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class RegisteredService {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String clientId;
    String owner;
    String name;
    @Temporal(TemporalType.TIMESTAMP)
    Date creationDate;
    String registrationAccessToken;
    String keyType;
    String url;
    String description;
    String frequency;
    @Convert(converter = StringListConverter.class)
    private List<String> target;
    String issuer;
    @Convert(converter = StringListConverter.class)
    private List<String> contacts;

    public RegisteredService() {
        this.creationDate = new Date();
    }

    public RegisteredService(String owner, String issuer) {
        this.owner = owner;
        this.issuer = issuer;
        this.creationDate = new Date();
    }

    public RegisteredService(RegisteredService service, String registrationAccessToken, String issuer) {
        this.clientId = service.getClientId();
        this.owner = service.getOwner();
        this.name = service.getName();
        this.creationDate = new Date();
        this.registrationAccessToken = registrationAccessToken;
        this.keyType = service.getKeyType();
        this.url = service.getUrl();
        this.description = service.getDescription();
        this.frequency = service.getFrequency();
        this.target = service.getTarget();
        this.issuer = issuer;
        this.contacts = service.getContacts();
    }

    public void setService(ServiceForm form, ServiceResponse response) {
        this.clientId = response.getClientId();
        this.name = form.getName();
        this.registrationAccessToken = response.getRegistrationAccessToken();
        this.keyType = form.getKeyType();
        this.url = form.getUrl();
        this.description = form.getDescription();
        this.frequency = form.getFrequency();
        this.target = form.getTarget();
        this.contacts = form.getContacts();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getRegistrationAccessToken() {
        return registrationAccessToken;
    }

    public void setRegistrationAccessToken(String registrationAccessToken) {
        this.registrationAccessToken = registrationAccessToken;
    }

    public String getKeyType() {
        return keyType;
    }

    public void setKeyType(String keyType) {
        this.keyType = keyType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public List<String> getTarget() {
        return target;
    }

    public void setTarget(List<String> target) {
        this.target = target;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public List<String> getContacts() {
        return contacts;
    }

    public void setContacts(List<String> contacts) {
        this.contacts = contacts;
    }
}
