package eu.dnetlib.developers.services;

import eu.dnetlib.developers.entities.RegisteredService;
import eu.dnetlib.developers.repositories.PersonalInfoDAO;
import eu.dnetlib.developers.repositories.RegisteredServiceDAO;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component("AuthorizationService")
public class AuthorizationService {

    private final RegisteredServiceDAO registeredServiceDAO;
    private final PersonalInfoDAO personalInfoDAO;

    @Autowired
    public AuthorizationService(RegisteredServiceDAO registeredServiceDAO, PersonalInfoDAO personalInfoDAO) {
        this.registeredServiceDAO = registeredServiceDAO;
        this.personalInfoDAO = personalInfoDAO;
    }

    public boolean isMyService(Long id) {
        OIDCAuthenticationToken authentication = (OIDCAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        RegisteredService service = this.registeredServiceDAO.findById(id).orElse(null);
        return service != null && service.getOwner().equals(authentication.getSub());
    }

    public boolean hasPersonalInfo() {
        OIDCAuthenticationToken authentication = (OIDCAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        return this.personalInfoDAO.findById(authentication.getSub()).orElseThrow(null) != null;
    }
}
