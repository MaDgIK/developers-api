package eu.dnetlib.developers.services;

import eu.dnetlib.developers.entities.PersonalInfo;
import eu.dnetlib.developers.exceptions.EntityNotFoundException;
import eu.dnetlib.developers.repositories.PersonalInfoDAO;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service()
public class PersonalInfoService {

    private PersonalInfoDAO dao;

    @Autowired
    public PersonalInfoService(PersonalInfoDAO dao) {
        this.dao = dao;
    }

    public List<PersonalInfo> getAll() {
        return this.dao.findAll();
    }

    public PersonalInfo save(PersonalInfo personalInfo) {
        return this.dao.save(personalInfo);
    }

    public PersonalInfo getPersonalInfo() {
        OIDCAuthenticationToken authentication = (OIDCAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        return this.dao.findById(authentication.getSub()).orElseThrow( () -> new EntityNotFoundException("Personal Info of user with id " + authentication.getSub() + " has not been found"));
    }

    public PersonalInfo savePersonalInfo(PersonalInfo personalInfo) {
        OIDCAuthenticationToken authentication = (OIDCAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        personalInfo.setId(authentication.getSub());
        return this.dao.saveAndFlush(personalInfo);
    }
}
