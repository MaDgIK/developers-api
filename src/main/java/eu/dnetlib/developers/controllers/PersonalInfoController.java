package eu.dnetlib.developers.controllers;

import eu.dnetlib.developers.entities.PersonalInfo;
import eu.dnetlib.developers.services.PersonalInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/personal/")
@CrossOrigin(origins = "*")
public class PersonalInfoController {

    PersonalInfoService service;

    @Autowired
    public PersonalInfoController(PersonalInfoService service) {
        this.service = service;
    }

    @PreAuthorize("hasAnyAuthority('PORTAL_ADMINISTRATOR')")
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<PersonalInfo>> getAll() {
        return ResponseEntity.ok(this.service.getAll());
    }

    @PreAuthorize("hasAnyAuthority('REGISTERED_USER')")
    @RequestMapping(value = "/my-info", method = RequestMethod.GET)
    public ResponseEntity<PersonalInfo> getMyPersonalInfo() {
        return ResponseEntity.ok(this.service.getPersonalInfo());
    }

    @PreAuthorize("hasAnyAuthority('REGISTERED_USER')")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ResponseEntity<PersonalInfo> savePersonalInfo(@RequestBody PersonalInfo personalInfo) {
        return ResponseEntity.ok(this.service.savePersonalInfo(personalInfo));
    }
}
