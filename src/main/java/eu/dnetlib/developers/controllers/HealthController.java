package eu.dnetlib.developers.controllers;

import eu.dnetlib.developers.configuration.GlobalVars;
import eu.dnetlib.developers.configuration.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class HealthController {
    private final Logger log = LogManager.getLogger(this.getClass());
    private final Properties properties;
    private final GlobalVars globalVars;

    @Autowired
    public HealthController(Properties properties, GlobalVars globalVars) {
        this.properties = properties;
        this.globalVars = globalVars;
    }

    @RequestMapping(value = {"", "/health_check"}, method = RequestMethod.GET)
    public String hello() {
        log.debug("Hello from Developers API service!");
        return "Hello from Developers API service!";
    }

    @PreAuthorize("hasAnyAuthority('PORTAL_ADMINISTRATOR')")
    @RequestMapping(value = "/health_check/advanced", method = RequestMethod.GET)
    public Map<String, String> checkEverything() {
        Map<String, String> response = new HashMap<>();
        response.put("developers.datasource.driver", properties.getDatasource().getDriver());
        response.put("developers.datasource.url", properties.getDatasource().getUrl());
        response.put("developers.datasource.username", properties.getDatasource().getUsername());
        response.put("developers.datasource.password", properties.getDatasource().getPassword());
        response.put("developers.issuer", properties.getIssuer());
        if(GlobalVars.date != null) {
            response.put("Date of deploy", eu.dnetlib.authentication.configuration.GlobalVars.date.toString());
        }
        if(globalVars.getBuildDate() != null) {
            response.put("Date of build", globalVars.getBuildDate());
        }
        if (globalVars.getVersion() != null) {
            response.put("Version", globalVars.getVersion());
        }
        return response;
    }
}
