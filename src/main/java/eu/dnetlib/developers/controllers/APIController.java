package eu.dnetlib.developers.controllers;

import eu.dnetlib.developers.dto.API;
import eu.dnetlib.developers.dto.APIsByIssuer;
import eu.dnetlib.developers.dto.CopyServices;
import eu.dnetlib.developers.dto.ServiceForm;
import eu.dnetlib.developers.services.APIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/apis")
@CrossOrigin(origins = "*")
public class APIController {

    @Autowired
    private APIService service;

    @Autowired
    public APIController(APIService service) {
        this.service = service;
    }

    @PreAuthorize("hasAnyAuthority('PORTAL_ADMINISTRATOR')")
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<APIsByIssuer>> getAll() {
        return ResponseEntity.ok(this.service.getAll());
    }

    @PreAuthorize("hasAnyAuthority('PORTAL_ADMINISTRATOR')")
    @RequestMapping(value = "/copy", method = RequestMethod.POST)
    public ResponseEntity<List<API>> copyServices(@RequestBody CopyServices services) {
        return ResponseEntity.ok(this.service.copyServices(services));
    }


    @PreAuthorize("hasAnyAuthority('REGISTERED_USER') && @AuthorizationService.hasPersonalInfo()")
    @RequestMapping(value = "/my-services", method = RequestMethod.GET)
    public ResponseEntity<List<API>> getMyServices() {
        return ResponseEntity.ok(this.service.getMyServices());
    }

    @PreAuthorize("hasAnyAuthority('REGISTERED_USER') && @AuthorizationService.hasPersonalInfo()")
    @RequestMapping(value = "/save/new", method = RequestMethod.POST)
    public ResponseEntity<API> create(@RequestBody ServiceForm form) {
        return ResponseEntity.ok(this.service.save(form));
    }

    @PreAuthorize("hasAnyAuthority('REGISTERED_USER') && @AuthorizationService.hasPersonalInfo() && @AuthorizationService.isMyService(#id)")
    @RequestMapping(value = "/save/{id}", method = RequestMethod.POST)
    public ResponseEntity<API> update(@PathVariable Long id, @RequestBody ServiceForm form) {
        return ResponseEntity.ok(this.service.save(form, id));
    }

    @PreAuthorize("hasAnyAuthority('PORTAL_ADMINISTRATOR') || (hasAnyAuthority('REGISTERED_USER') && @AuthorizationService.hasPersonalInfo() && @AuthorizationService.isMyService(#id))")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable Long id) {
        this.service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
