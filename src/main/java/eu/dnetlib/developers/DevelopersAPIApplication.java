package eu.dnetlib.developers;

import eu.dnetlib.authentication.configuration.AuthenticationConfiguration;
import eu.dnetlib.developers.configuration.APIProperties;
import eu.dnetlib.developers.configuration.GlobalVars;
import eu.dnetlib.developers.configuration.Properties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication(scanBasePackages = {"eu.dnetlib.developers"})
@PropertySources({
        @PropertySource("classpath:authentication.properties"),
        @PropertySource("classpath:developers.properties"),
        @PropertySource(value = "classpath:dnet-override.properties", ignoreResourceNotFound = true)
})
@Import({AuthenticationConfiguration.class})
@EnableConfigurationProperties({Properties.class, APIProperties.class, GlobalVars.class})
public class DevelopersAPIApplication {

    public static void main(String[] args) {
        SpringApplication.run(DevelopersAPIApplication.class, args);
    }

}
