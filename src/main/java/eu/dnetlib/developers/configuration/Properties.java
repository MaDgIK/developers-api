package eu.dnetlib.developers.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("developers")
public class Properties {
    Datasource datasource;
    String issuer;

    public Properties() {
    }

    public Datasource getDatasource() {
        return datasource;
    }

    public void setDatasource(Datasource datasource) {
        this.datasource = datasource;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }
}
