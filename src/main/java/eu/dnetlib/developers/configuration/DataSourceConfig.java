package eu.dnetlib.developers.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {

    Datasource datasource;

    @Autowired
    public DataSourceConfig(Properties properties) {
        this.datasource = properties.getDatasource();
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(this.datasource.getDriver());
        dataSource.setUrl(this.datasource.getUrl());
        dataSource.setUsername(this.datasource.getUsername());
        dataSource.setPassword(this.datasource.getPassword());
        return dataSource;
    }
}
