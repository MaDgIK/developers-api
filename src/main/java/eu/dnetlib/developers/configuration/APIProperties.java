package eu.dnetlib.developers.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("api")
public class APIProperties {

    private String title;
    private String description;
    private String version;

    public APIProperties() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
