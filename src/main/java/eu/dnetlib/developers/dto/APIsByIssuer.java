package eu.dnetlib.developers.dto;

import java.util.List;

public class APIsByIssuer {
    String issuer;
    List<API> apis;

    public APIsByIssuer() {
    }

    public APIsByIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public List<API> getApis() {
        return apis;
    }

    public void setApis(List<API> apis) {
        this.apis = apis;
    }
}
