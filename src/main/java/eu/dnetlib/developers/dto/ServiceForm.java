package eu.dnetlib.developers.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.URL;

import java.util.List;

public class ServiceForm {
    @JsonProperty(required = true)
    private String name;
    @URL(message = "Service URL must be a valid URL")
    private String url;
    @URL(message = "Logo URL must be a valid URL")
    private String logoURL;
    private String keyType;
    @URL(message = "uri must be a valid URL", protocol = "https")
    private String uri;
    private String value;
    @JsonIgnore
    private Jwks jwks;
    private String description;
    private String frequency;
    private List<String> target;
    private List<String> contacts;


    public ServiceForm() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogoURL() {
        return logoURL;
    }

    public void setLogoURL(String logoURL) {
        this.logoURL = logoURL;
    }

    public String getKeyType() {
        return keyType;
    }

    public void setKeyType(String keyType) {
        this.keyType = keyType;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Jwks getJwks() {
        return jwks;
    }

    public void setJwks(Jwks jwks) {
        this.jwks = jwks;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public List<String> getTarget() {
        return target;
    }

    public void setTarget(List<String> target) {
        this.target = target;
    }

    public List<String> getContacts() {
        return contacts;
    }

    public void setContacts(List<String> contacts) {
        this.contacts = contacts;
    }
}
