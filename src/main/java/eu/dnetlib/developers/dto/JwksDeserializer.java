package eu.dnetlib.developers.dto;

import com.google.gson.*;

import java.lang.reflect.Type;

public class JwksDeserializer implements JsonDeserializer<Jwks> {

    @Override
    public Jwks deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext)
            throws JsonParseException {

        JsonObject jsonObject = jsonElement.getAsJsonObject();
        if (jsonObject == null) throw new JsonParseException("Jwks not valid.");
        JsonArray jsonArray = jsonObject.getAsJsonArray("keys");

        if (jsonArray == null ) throw new JsonParseException("Jwks not valid.");

        Jwks jwks = new Jwks();
        Key[] keys = new Key[jsonArray.size()];

        Key key = null;
        for (int i = 0; i < jsonArray.size(); i++) {
            key = new Key();
            JsonElement je = jsonArray.get(i);

            if (je == null) throw new JsonParseException("Jwks not valid.");
            if (je.getAsJsonObject().get("kty")==null) throw new JsonParseException("Jwks not valid.");
            key.setKty(je.getAsJsonObject().get("kty").getAsString());

            if (je.getAsJsonObject().get("e")==null) throw new JsonParseException("Jwks not valid.");
            key.setE(je.getAsJsonObject().get("e").getAsString());

            if (je.getAsJsonObject().get("kid")==null) throw new JsonParseException("Jwks not valid.");
            key.setKid(je.getAsJsonObject().get("kid").getAsString());

            if (je.getAsJsonObject().get("alg")==null) throw new JsonParseException("Jwks not valid.");
            key.setAlg(je.getAsJsonObject().get("alg").getAsString());

            if (je.getAsJsonObject().get("n")==null) throw new JsonParseException("Jwks not valid.");
            key.setN(je.getAsJsonObject().get("n").getAsString());
            keys[i] = key;
        }

        jwks.setKeys(keys);
        return jwks;
    }
}
