package eu.dnetlib.developers.dto;

import java.util.List;

public class CopyServices {
    public String issuer;
    public List<CopyService> services;

    public CopyServices() {
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public List<CopyService> getServices() {
        return services;
    }

    public void setServices(List<CopyService> services) {
        this.services = services;
    }
}
