package eu.dnetlib.developers.dto;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.Serializable;

public class ServiceResponse implements Serializable {
    String client_id;
    Long client_id_issued_at;
    String client_secret;
    Long client_secret_expires_at;
    String registration_access_token;
    String registration_client_uri;
    String[] redirect_uris;
    String client_name;
    String logo_uri;
    String policy_uri;
    String[] contacts;
    String[] grant_types;
    String token_endpoint_auth_method;
    String token_endpoint_auth_signing_alg;
    String scope;
    String jwks_uri;
    Jwks jwks;


    public String getClientId() {
        return client_id;
    }

    public Long getClientIdIssuedAt() {
        return client_id_issued_at;
    }

    public String getClientSecret() {
        return client_secret;
    }

    public Long getClientSecretExpiresAt() {
        return client_secret_expires_at;
    }

    public String getRegistrationAccessToken() {
        return registration_access_token;
    }

    public String getRegistrationClientUri() {
        return registration_client_uri;
    }

    public String[] getRedirectUris() {
        return redirect_uris;
    }

    public String getClientName() {
        return client_name;
    }


    public String getLogoUri() {
        return logo_uri;
    }

    public String getPolicyUri() {
        return policy_uri;
    }

    public String[] getContacts() {
        return contacts;
    }

    public String[] getGrantTypes() {
        return grant_types;
    }

    public String getTokenEndpointAuthMethod() {
        return token_endpoint_auth_method;
    }

    public String getTokenEndpointAuthSigningAlg() {
        return token_endpoint_auth_signing_alg;
    }

    public String getScope() {
        return scope;
    }

    public String getJwksUri() {
        return jwks_uri;
    }

    public Jwks getJwks() {
        return jwks;
    }

    public String toJson() {
        GsonBuilder builder = new GsonBuilder();
        builder.serializeNulls();
        Gson gson = builder.create();
        return gson.toJson(this);
    }

    public static ServiceResponse fromString(String response) {
        return new Gson().fromJson(response, ServiceResponse.class);
    }
}
