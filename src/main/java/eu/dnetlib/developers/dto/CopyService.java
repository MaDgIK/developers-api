package eu.dnetlib.developers.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CopyService {
    @JsonProperty("client_id")
    private String clientId;
    @JsonProperty("registration_access_token")
    private String registrationAccessToken;

    public CopyService() {
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getRegistrationAccessToken() {
        return registrationAccessToken;
    }

    public void setRegistrationAccessToken(String registrationAccessToken) {
        this.registrationAccessToken = registrationAccessToken;
    }
}
