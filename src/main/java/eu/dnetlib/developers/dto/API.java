package eu.dnetlib.developers.dto;

import eu.dnetlib.developers.entities.RegisteredService;

public class API {
    private RegisteredService service;
    private ServiceResponse details;

    public API() {
    }

    public RegisteredService getService() {
        return service;
    }

    public void setService(RegisteredService service) {
        this.service = service;
    }

    public ServiceResponse getDetails() {
        return details;
    }

    public void setDetails(ServiceResponse details) {
        this.details = details;
    }
}
