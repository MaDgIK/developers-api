package eu.dnetlib.developers.dto;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

public class ServiceRequest {
    String client_name;
    String client_id;
    String logo_uri;
    String policy_uri;
    String[] contacts;
    String[] redirect_uris = new String[]{};
    String[] grant_types = new String[] {"client_credentials"};
    String token_endpoint_auth_method = "private_key_jwt";
    String token_endpoint_auth_signing_alg = "RS256";
    String jwks_uri;
    Jwks jwks;

    public String getClientName() {
        return client_name;
    }

    public void setClientName(String clientName) {
        this.client_name = clientName;
    }

    public String getClientId() {
        return client_id;
    }

    public void setClientId(String clientId) {
        this.client_id = clientId;
    }

    public String[] getRedirectUris() {
        return redirect_uris;
    }

    public void setRedirectUris(String[] redirectUris) {
        this.redirect_uris = redirectUris;
    }

    public String getLogoUri() {
        return logo_uri;
    }

    public void setLogoUri(String logoUri) {
        this.logo_uri = logoUri;
    }

    public String getPolicyUri() {
        return policy_uri;
    }

    public void setPolicyUri(String policyUri) {
        this.policy_uri = policyUri;
    }

    public String[] getContacts() {
        return contacts;
    }

    public void setContacts(List<String> contacts) {
        this.contacts = new String[contacts.size()];
        contacts.toArray(this.contacts);
    }

    public String[] getGrantTypes() {
        return grant_types;
    }

    public void setGrantTypes(String[] grantTypes) {
        this.grant_types = grantTypes;
    }

    public String getToken_endpoint_auth_method() {
        return token_endpoint_auth_method;
    }

    public void setToken_endpoint_auth_method(String token_endpoint_auth_method) {
        this.token_endpoint_auth_method = token_endpoint_auth_method;
    }

    public String getTokenEndpointAuthSigningAlg() {
        return token_endpoint_auth_signing_alg;
    }

    public void setTokenEndpointAuthSigningAlg(String tokenEndpointAuthSigningAlg) {
        this.token_endpoint_auth_signing_alg = tokenEndpointAuthSigningAlg;
    }

    public String getJwksUri() {
        return jwks_uri;
    }

    public void setJwksUri(String jwksUri) {
        this.jwks_uri = jwksUri;
    }

    public Jwks getJwks() {
        return jwks;
    }

    public void setJwks(Jwks jwks) {
        this.jwks = jwks;
    }

    public String toJson() {
        GsonBuilder builder = new GsonBuilder();
        builder.serializeNulls();
        Gson gson = builder.create();
        return gson.toJson(this);
    }

    public static ServiceRequest createServiceRequest(String client_id, String name, String logoURL, List<String> contacts) {
        ServiceRequest request = new ServiceRequest();
        request.setClientId(client_id);
        request.setClientName(name);
        request.setContacts(contacts);
        request.setToken_endpoint_auth_method("client_secret_basic");
        request.setTokenEndpointAuthSigningAlg(null);
        request.setLogoUri(logoURL);
        return request;
    }

    public static ServiceRequest createServiceRequest(String client_id, String name, String logoURL, List<String> contacts, String uri) {
        ServiceRequest request = new ServiceRequest();
        request.setClientId(client_id);
        request.setClientName(name);
        request.setContacts(contacts);
        request.setJwksUri(uri);
        request.setLogoUri(logoURL);
        return request;
    }

    public static ServiceRequest createServiceRequest(String client_id, String name, String logoURL, List<String> contacts, Jwks jwks) {
        ServiceRequest request = new ServiceRequest();
        request.setClientId(client_id);
        request.setClientName(name);
        request.setContacts(contacts);
        request.setJwks(jwks);
        request.setLogoUri(logoURL);
        return request;
    }
}
