package eu.dnetlib.developers.dto;

import java.io.Serializable;

public class Jwks implements Serializable {
    Key[] keys;

    public Key[] getKeys() {
        return keys;
    }

    public void setKeys(Key[] keys) {
        this.keys = keys;
    }

    public boolean isValid() {
        return this.keys != null && this.keys.length > 0;
    }
}
