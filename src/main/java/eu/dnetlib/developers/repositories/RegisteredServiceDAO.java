package eu.dnetlib.developers.repositories;

import eu.dnetlib.developers.entities.RegisteredService;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RegisteredServiceDAO extends CrudRepository<RegisteredService, Long> {

    List<RegisteredService> findAll();

    @Query("select distinct rs.issuer from RegisteredService rs where rs.issuer != :#{#issuer}")
    List<String> findAllIssuersExceptActive(@Param("issuer") String issuer);

    Optional<RegisteredService> findById(Long id);

    Optional<RegisteredService> findByClientIdAndIssuer(String clientId, String issuer);

    List<RegisteredService> findAllByIssuerOrderByOwnerAsc(String issuer);

    List<RegisteredService> findAllByOwnerAndIssuerOrderByCreationDateAsc(String owner, String issuer);
}
