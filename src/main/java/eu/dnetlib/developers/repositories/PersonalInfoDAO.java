package eu.dnetlib.developers.repositories;

import eu.dnetlib.developers.entities.PersonalInfo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonalInfoDAO extends CrudRepository<PersonalInfo, Long> {

    List<PersonalInfo> findAll();

    PersonalInfo saveAndFlush(PersonalInfo personalInfo);

    Optional<PersonalInfo> findById(String id);
}
